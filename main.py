import string
import find_duplicate_files
import os
import pyexiv2 as pe
from datetime import datetime, timezone
import pytz
import tkinter as tk
from tkinter import ttk
from tkinter import *
from tkinter import filedialog
import csv
import xlsxwriter
from tkinter import messagebox
import shutil
month = {'01': 'Janvier', '02': 'Fevrier', '03': 'Mars', '04': 'Avril', '05': 'Mai', '06': 'Juin', '07': 'Juillet',
         '08': 'Aout', '09': 'Septembre', '10': 'Octobre', '11': 'Novembre', '12': 'Decembre'}


def find_duplicate():
    global directory_in, directory_out
    if directory_in is None or directory_out is None:
        label_info.config(text=label_info["text"] + "\n Choisissez les dossiers avant !")
    else:
        global duplicate_files, max_iter
        duplicate_files = find_duplicate_files.find_duplicate_files(directory_in)
        max_iter = 0
        fic_name=os.path.join(directory_out, "duplicates.html")
        fic = open(fic_name, "w", encoding="utf-8")
        fic.write("<!DOCTYPE html> <html> <head><meta charset='UTF-8'>"
                "<title>Liste des fichiers du repertoire : " + directory_in + "</title></head><body>")
        fic.write("<ul>")
        for fi in duplicate_files:
            fic.write("<li>"+str(len(fi))+ " occurences : </li><ul>")
            for f in fi:
                fic.write("<li><a target='_blank', href='file:///"+str(f)+"'>"+str(f)+"</a></li>")
            fic.write("</ul>")
            if len(fi) > max_iter:
                max_iter = len(fi)
        fic.write("</ul>")
        label_info.config(text=label_info["text"] + "\n " + str(
            len(duplicate_files)) + " doublons trouvé(s), Nombre max d'occurence : " + str(max_iter))
        label_info.config(text=label_info["text"] + "\n Un fichier recap a été créé au chemin : "+fic_name)
        fic.close()
        return duplicate_files, max_iter


def remove_duplicate():
    global duplicate_files, directory_in
    if directory_in is None:
        label_info.config(text=label_info["text"] + "\n Choisissez un dossier avant !")
    else:
        if duplicate_files is None:
            label_info.config(text=label_info["text"] + "\n Il faut d'abord chercher les doublons ;)")
        else:
            if len(duplicate_files) == 0:
                label_info.config(text=label_info["text"] + "\n Il n'y a pas de doublons a enlever !")
            nb_removed = 0
            for duplicate in duplicate_files:
                if which_media(duplicate[0]) is not None:
                    for i in range(len(duplicate) - 1):
                        try:
                            os.remove(duplicate[i + 1])
                            nb_removed = nb_removed + 1
                        except PermissionError:
                            pass
            label_info.config(text=label_info["text"] + "\n " + str(nb_removed) + " fichiers supprimés")
            return nb_removed


def which_media(file):
    img_fm = (".tif", ".tiff", ".jpg", ".jpeg", ".gif", ".png", ".eps",
              ".raw", ".cr2", ".nef", ".orf", ".sr2", ".bmp", ".ppm", ".heif", '.tbm', 'jpe',".html",".svg")
    vid_fm = (".flv", ".avi", ".mp4", ".3gp", ".mov", ".webm", ".ogg", ".qt", ".avchd", ".m2t", ".m2ts", ".mpeg",".mts")
    aud_fm = (".flac", ".mp3", ".wav", ".wma", ".aac")
    text_fm =(".doc",'.docx','.txt','.odt')
    pdf_fm = (".pdf")
    tableur_fm = (".xls",".xlsx",".csv")
    powerpoint_fm = (".ppt",".pptx")
    archive_fm = (".zip",".rar")
    media_fms = {"Image": img_fm, "Video": vid_fm, "Audio": aud_fm,"DocText":text_fm,"Tableur": tableur_fm, "PowerPt": powerpoint_fm,"DocPDF": pdf_fm,"Archive":archive_fm}
    for media in media_fms.keys():
        if file.lower().endswith(media_fms[media]):
            return media
    return None


def list_files():
    global doc_name_var, directory_out, directory_in, doc_name_var
    path = directory_in
    want_media= list_media.get()
    want_document = list_document.get()
    want_all = list_all.get()
    if not (want_all or want_document or want_media):
        label_info.config(text=label_info["text"] + "\n Veuillez choisir au moins une catégorie de fichiers à lister ")
    else:
        if directory_in is None or directory_out is None or not doc_name_var.get():
            label_info.config(text = label_info["text"] + "\n Veuillez rentrer les chemin d'accès et/ou le nom du fichier ")
        else:
            list_files_wanted=[]
            if want_media:
                list_files_wanted.extend(["Image","Video","Audio"])
            if want_document:
                list_files_wanted.extend(["DocText", "Tableur", "DocPDF","PowerPt"])

            fic = os.path.join(directory_out, doc_name_var.get())
            f=open(fic, "w", encoding="utf-8")
            if ".csv" in fic:
                csv_file= csv.writer(f)
                csv_file.writerow(["Type de fichier","Chemin d'accès","Taille en octet","Date de création","Date de dernière modification"])
            if ".xls"in fic or ".xlsx" in fic:
                row = column= 0
                workbook =  xlsxwriter.Workbook(fic)
                worksheet= workbook.add_worksheet()
                for item in ["Type de fichier", "Chemin d'acces", "Taille en octet", "Date de creation", "Date de derniere modification"]:
                    worksheet.write(row, column, item)
                    column = column + 1
                row=row+1
            if ".html" in fic:
                f = open(fic,"w", encoding="utf-8")
                f.write("<!DOCTYPE html> <html> <head><meta charset='UTF-8'>"
                        "<title>Liste des fichiers du repertoire : "+directory_in+"</title></head><body>"
                            "<table><thead><tr><th colspan='5'>Liste des fichiers</th></tr></thead><tbody><tr>")
                for item in ["Type de fichier", "Chemin d'acces", "Taille en octet", "Date de creation", "Date de derniere modification"]:
                    f.write("<th>"+item+"</th>")
                f.write("</tr>")
            maxlength=0
            nbfiles = 0
            total_size=0
            size_wanted=0
            nbfiles_total=0
            for paths, currentDirectory, files in os.walk(path):
                for file_name in files:
                    file = os.path.join(paths, file_name)
                    try:
                        total_size+=os.stat(file).st_size
                    except Exception as e:
                        pass
                    nbfiles_total+=1
                    med=which_media(file)
                    if med is None:
                        med="Autre "
                    if  want_all or med in list_files_wanted:
                        try:
                            size_wanted+=os.stat(file).st_size
                        except Exception as e:
                            pass
                        if len(file)>maxlength:
                            maxlength= len(med+file)

            for paths, currentDirectory, files in os.walk(path):
                for file_name in files:
                    file = os.path.join(paths, file_name)
                    med = which_media(file_name)
                    if want_all or (med in list_files_wanted):
                        try:
                            if med is None:
                                med = "Autre "
                            nbfiles= nbfiles+1
                            stat = os.stat(file)
                            dateMod = datetime.fromtimestamp(stat.st_mtime, tz=pytz.timezone("Europe/Paris"))
                            dateCrea = datetime.fromtimestamp(stat.st_ctime, tz=pytz.timezone("Europe/Paris"))
                            size = stat.st_size
                            if(size>1000000):
                                str_size=str(round((size/1000000),3))+" MO"
                            else:
                                str_size = str(round((size/1000),3))+" kO"
                            if '.csv' in fic:
                                csv_file.writerow([med,file, size,str(dateCrea), str(dateMod)])

                            elif ".xls"in fic or ".xlsx" in fic:
                                column=0
                                for item in [med, file, size, str(dateCrea), str(dateMod)]:
                                    if item != file:
                                        worksheet.write(row, column, item)
                                        column = column+1
                                    else:
                                        worksheet.write_url(row, column, url="file:///"+str(item), string=str(item))
                                        column = column +1
                                row=row+1
                            elif ".html" in fic:
                                f.write("<tr>")
                                for item in [med, file, size,str(dateCrea), str(dateMod)]:
                                    if item == file:
                                        f.write("<td><a target='_blank', href='file:///"+str(item)+"'>"+str(item)+"</a></td>")
                                    else:
                                        f.write("<td>"+str(item)+"</td>")
                                f.write("</tr>")
                            else:
                                f.write(med +": "+ file+((maxlength-len(med+file))+1)*" "+"/ taille :"+str_size+" / date de Création : "+str(dateCrea)+"/ date de modification : "+str(dateMod)+"\n")
                        except Exception as e:
                            pass
            label_info.config(
                text=label_info["text"] + "\n"+str(nbfiles)+" fichiers "
                      "("+str(round((size_wanted/1000000),2))+"MO) ont été recensés sur un total de "+str(nbfiles_total)+
                            " fichiers ("+str(round((total_size/1000000),2))+"MO). \n Le fichier recap a bien été créé au chemin : " + fic)

            if '.csv' in fic:
                csv_file.close()

            elif ".xls" in fic or ".xlsx" in fic:
                workbook.close()
            elif ".html" in fic:
                f.write("</tbody></table><style>table,td {border: 1px solid #333;}thead,tfoot {background-color: #333;color: #fff;}</style></body></html>")
                f.close()
            else:
                f.close()


def get_date_heure_subjects(file, med):
    if med == "Image":
        # On va chercher les donnees exif de chaque image
        try:
            with pe.Image(file) as img:
                exif = img.read_exif()
                xmp = img.read_xmp()

            
            dateheure = exif.get('Exif.Photo.DateTimeDigitized', None)
            if dateheure is not None:
                dateheure = dateheure.split(' ')
                date = dateheure[0].split(':')
                heure = dateheure[1].split(':')
            else:
                try:
                    stat = os.stat(file)
                    date = datetime.fromtimestamp(stat.st_mtime, tz=timezone.utc)
                    dateheure = str(date).split('+')[0].split(' ')
                    date = dateheure[0].split('-')
                    heure = dateheure[1].split(':')
                except IndexError:
                    date = heure = None
            subjects = xmp.get('Xmp.dc.subject', [])

        except Exception as e:
            date = heure = None    
            subjects = []        
    else:
        try:
            stat = os.stat(file)
            date = datetime.fromtimestamp(stat.st_mtime, tz=timezone.utc)
            dateheure = str(date).split('+')[0].split(' ')
            date = dateheure[0].split('-')
            heure = dateheure[1].split(':')
            subjects = []
        except IndexError:
            date = heure = None

    return date, heure, subjects


def sort_media():
    global directory_in, directory_out, duplicate_files, split_in_days_var, rename_file_var, split_media_var, copy_file_var
    path = directory_in
    out = directory_out
    copy_file= copy_file_var.get()
    different_partition = directory_in.split(":")[0] != directory_out.split(":")[0]
    nb_moved = 0
    nb_error=0
    if directory_in is None or directory_out is None:
        label_info.config(text=label_info["text"] + "\n Choisissez les dossiers avant !")
    else:
        split_in_days = split_in_days_var.get()
        rename = rename_file_var.get()
        split_media = split_media_var.get()
        pe.set_log_level(4)

        can_copy=True
        if  different_partition or copy_file:
            if different_partition:
                label_info.config(text=label_info["text"] + "\n Les dossiers ne sont pas sur la même partition, une copie sera necessaire.")
                label_info.config(text=label_info[
                                           "text"] + "\n Calcul de la taille des fichiers à copier ... ")
            else :
                label_info.config(text=label_info[
                                           "text"] + "\n Calcul de la taille des fichiers à copier ... ")
            total_size=0
            for paths, currentDirectory, files in os.walk(path):
                for file_name in files:
                    file = os.path.join(paths, file_name)
                    if which_media(file) is not None:
                        try:
                            total_size += os.stat(file).st_size
                        except Exception as e:
                            pass
            total, used, free = shutil.disk_usage(directory_out[:2])
            if free>total_size+3000000000:
                label_info.config(text=label_info[
                                           "text"] + "\n Il y a "+ str(round((total_size/(2**30)),2))+" GO de données à copier,\n "
                                                                                      "le disque contient "+str(free//(2**30))+" GO d'espace dispo.")
                can_copy = messagebox.askyesno('confirmation', 'Voulez-vous continuer ?')
                currentpercentage = 0

            else:
                label_info.config(text=label_info[
                                           "text"] + "\n Il n'y a pas assez d'espace disponible :'( ")
                can_copy=False
            if not can_copy:
                label_info.config(text=label_info["text"] + "\n Aucun tri n'a été effectué")
        if can_copy:
            for paths, currentDirectory, files in os.walk(path):
                for file_name in files:
                    file = os.path.join(paths, file_name)
                    med = which_media(file)
                    if med is not None:
                        date, heure, subjects = get_date_heure_subjects(file, med)
                        try :
                            if not split_in_days:
                                directory = out + "\\" + date[0] + "\\" + month[date[1]] + "\\" + med * split_media
                            else:
                                directory = out + "\\" + date[0] + "\\" + month[date[1]] + "\\" + date[2] + "\\" + med * split_media
                        except Exception as e:
                            directory = out + "\\" + "Autre" + "\\" + med * split_media
                        os.makedirs(directory, exist_ok=True)

                        fname, file_extension = os.path.splitext(file_name)
                        if rename:
                            # new_name = fname+date[2] + "_" + date[1] + "_" + date[0] + "_" + heure[0] + '_' + heure[1] + '_' + \
                            #            heure[2] + file_extension
                            new_name = fname + ("_"+"_".join(sorted(list(set(subjects)))) if len(subjects) > 0 else "") + file_extension
                        else:
                            new_name = file_name
                        new_file = os.path.join(directory, new_name)
                        i = 1
                        while os.path.isfile(new_file):
                            fname, file_extension = os.path.splitext(new_name)
                            if rename:
                                # new_name = fname+ "_"+date[2] + "_" + date[1] + "_" + date[0] + "_" + heure[0] + '_' + heure[1] + '_' + \
                                #            heure[
                                #                2] + "(" + str(i) + ")" + file_extension
                                new_name = fname + "_".join(sorted(subjects)) + "(" + str(i) + ")" + file_extension
                            else:
                                if i > 1:
                                    new_name = fname[:-3] + "(" + str(i) + ")" + file_extension
                                else:
                                    new_name = fname + "(" + str(i) + ")" + file_extension

                            new_file = os.path.join(directory, new_name)
                            i = i + 1

                        try:
                            if copy_file:
                                shutil.copy(file, new_file)
                            else:
                                shutil.move(file, new_file)
                            nb_moved = nb_moved + 1
                        except:
                            label_info.config(text=label_info["text"] + "\n Il y a eu un problème avec le fichier :\n "+file+" ---> "+new_file+"\n")
                            nb_error= nb_error+1
            if different_partition or copy_file:
                label_info.config(text=label_info["text"] + "\n Les fichiers ont été triées avec "+str(nb_error)+" erreur(s) \n" + str(
                    nb_moved) + " fichier(s) ont été copié(s)" )
            else:
                label_info.config(text=label_info["text"] + "\n Les fichiers ont été triées avec "+str(nb_error)+" erreur(s) \n" + str(
                                    nb_moved) + " fichier(s) ont été déplacé(s)" )


def get_directory_in():
    global directory_in
    directory_in = filedialog.askdirectory()
    directoryin_name.config(text=directory_in)


def get_directory_out():
    global directory_out
    directory_out = filedialog.askdirectory()
    directoryout_name.config(text=directory_out)


def create_top_frame(container):
    global directoryin_name, directoryout_name
    frame = ttk.Frame(container)

    ttk.Label(frame, text='Dossier avec les fichiers :').grid(column=0, row=0,sticky=tk.E)
    directoryin_name = ttk.Label(frame, text="Pas encore choisi...", relief=SUNKEN, width=60)
    directoryin_name.focus()
    directoryin_name.grid(column=1, row=0, sticky=tk.W)

    button = Button(frame, text="Cliquez pour choisir", command=get_directory_in).grid(column=2, row=0)

    ttk.Label(frame, text='Dossier où déposer les fichiers :').grid(column=0, row=1, sticky=tk.E)
    directoryout_name = ttk.Label(frame, text="Pas encore choisi...", relief=SUNKEN, width=60)
    directoryout_name.focus()
    directoryout_name.grid(column=1, row=1, sticky=tk.W)

    button2 = Button(frame, text="Cliquez pour choisir",
                     command=get_directory_out).grid(column=2, row=1)
    
    ttk.Label(frame, text='Lister les fichiers', font=("Calibri Light", 15, "bold"),).grid(column=1, columnspan=1, row=2, sticky=tk.N)

    c1 = Checkbutton(frame, text="Lister les fichiers (doc, xls, pdf, ppt...)", variable=list_document,
                     onvalue=1, offvalue=0)
    c1.grid(column=0, row=3)

    c2 = Checkbutton(frame, text="Lister les médias (image, audio, video)", variable=list_media,
                     onvalue=1, offvalue=0)
    c2.grid(column=1, row=3)
    c3 = Checkbutton(frame, text="Lister tous les fichiers", variable=list_all,
                     onvalue=1, offvalue=0)
    c3.grid(column=2, row=3)

    label_entry = Label(frame,
                        text="Choisissez le nom du fichier à créer (document texte enregistré dans le dossier de sortie) :  ").grid(
        column=0, row=4, columnspan=2, stick=tk.W, padx=25, pady=5)
    entry_doc_name = Entry(frame, textvariable=doc_name_var).grid(column=1, row=4, stick=tk.E)

    button7 = Button(frame, text="Lister les fichiers",
                     command=list_files).grid(column=2, row=4)

    for widget in frame.winfo_children():
        widget.grid(padx=5, pady=10)

    return frame


def create_checkbox_frame(container):
    frame = ttk.Frame(container)

    ttk.Label(frame, text='Trier les fichiers', font=("Calibri Light", 15, "bold"), ).grid(column=0, sticky=tk.E, columnspan=1,
                                                                                            row=0)
    c1 = Checkbutton(frame, text="Creer un dossier par jour", variable=split_in_days_var,
                     onvalue=1, offvalue=0)
    c1.grid(column=0, row=1, sticky=tk.W)
    c2 = Checkbutton(frame, text='Séparer selon les types de fichiers', variable=split_media_var, onvalue=1, offvalue=0)
    c2.grid(column=1, row=1, sticky=tk.W)
    c3 = Checkbutton(frame, text='Renommer les fichiers selon ancienNom_personne1_personne2', variable=rename_file_var,
                     onvalue=1,
                     offvalue=0)
    c3.grid(column=0, row=2, sticky=tk.W)

    c3 = Checkbutton(frame, text='Copier les fichiers au lieu de les déplacer', variable=copy_file_var,
                     onvalue=1,
                     offvalue=0)
    c3.grid(column=1, row=2)

    for i, widget in enumerate(frame.winfo_children()):
        widget.grid(padx=5, pady=10)

    return frame


def create_button_frame(container):
    frame = ttk.Frame(container)

    button3 = Button(frame, text="Chercher les doublons",
                         command=find_duplicate).grid(column=0, row=0)
    button4 = Button(frame, text="Supprimer les doublons",
                         command=remove_duplicate).grid(column=1, row=0)

    button5 = Button(frame, text="Trier les fichiers",
                     command=sort_media).grid(column=2, row=0)

    for i, widget in enumerate(frame.winfo_children()):
        widget.grid(padx=5, pady=10)

    return frame


def create_information_frame(container):
    global label_info
    frame = Frame(container)
    frame_in = Frame(frame, bd=4, relief=RIDGE)
    button6 = Button(frame, text="Effacer les informations",
                     command=lambda: (label_info.config(text="Informations: :"))).grid(column=0, row=0)

    label_info = ttk.Label(frame_in, text="Informations :")
    frame_in.grid(column=0, row=1)
    label_info.grid(column=0, row=1)

    for i, widget in enumerate(frame.winfo_children()):
        widget.grid(padx=5, pady=10)
    return frame


def create_main_window():
    global directory_in, directory_out, duplicate_files, split_in_days_var, rename_file_var, split_media_var, \
        label_info, doc_name_var, copy_file_var, info_frame, list_document, list_media, list_all
    directory_in = None
    directory_out = None
    duplicate_files = None

    root = tk.Tk()

    split_in_days_var = tk.IntVar()
    rename_file_var = tk.IntVar()
    split_media_var = tk.IntVar()
    copy_file_var=tk.IntVar()
    list_document = tk.IntVar()
    list_media = tk.IntVar()
    list_all = tk.IntVar()
    doc_name_var = StringVar()
    root.title("Superbe application garantie sans bug de Yoann")
    root.geometry("960x680")
    root.minsize(510, 360)

    try:
        # windows only (remove the minimize/maximize button)
        root.attributes('-toolwindow', True)
    except TclError:
        print('Not supported on your platform')

    # layout on the root window

    frame_title = tk.Frame(root, bg="#71B4CA", bd=2)
    label_title = tk.Label(frame_title, text="Bienvenue dans l'app de gestion de fichiers !", font=("Calibri Light", 40), bg="#71B4CA",
                           fg="white").pack()
    frame_title.grid(row=0, columnspan=2, sticky=tk.EW)

    top_frame = create_top_frame(root).grid(column=0, row=1)

    check_box_frame= create_checkbox_frame(root).grid(column=0, row=2)

    button_frame= create_button_frame(root).grid(column=0, row=3)

    info_frame= create_information_frame(root).grid(column=0, row=4)
    root.mainloop()

if __name__ == "__main__":
    create_main_window()