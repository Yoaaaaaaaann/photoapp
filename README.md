# photoApp

## Getting started

photoApp is a small GUI app that help you sort you thousands of messy files in your computer.
The app can :
1) List all files in a specified directory and create a liste with the files and its metadata.
The file can be an doc, xls or html (on which you can click on the file to see what it is)
2) Detect the duplicated files on a specified directory
3) Delete those duplicates
4) Sort you files according to the year, the month and, if you want, the day and the media type.
5) You can also choose to rename all the pictures by the name : "dd_mm_yyyy_hh_mm_ss"


## How to launch the app

You have to install python if it's not already done.
Then you can do :
```
pip install -r requirements.txt
py main.py
```
